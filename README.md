<img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" align="left" width="144px" height="144px"/>

#### App Manager
> A plugin for [Oh My Fish][omf-link].

[![MIT License](https://img.shields.io/badge/license-MIT-007EC7.svg?style=flat-square)](/LICENSE)
[![Fish Shell Version](https://img.shields.io/badge/fish-v2.2.0-007EC7.svg?style=flat-square)](https://fishshell.com)
[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-007EC7.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

<br/>

# Introduction

simple aurman/pacman wrapper for daily use


## Install

```fish
$ omf install https://gitlab.com/merrickluo/omf-plugin-app
```


## Usage

```fish
$ app help                           # show this message
$ app add package1 [pacakge2]        # install pacakges
$ app update                         # check for updates
$ app upgrade [pacakge1 package2]    # upgrade packages (omit params to upgrade whole system)
$ app remove pacakge1 [package2]     # remove pacakge
$ app search package                 # search package
$ app list [package]                 # list installed packages (omit params for all)
$ app which filename                 # findout which package provides file
$ app cask [command]                 # brew cask support [osx only]
```


# License

[MIT][mit] © [A.I.][author] et [al][contributors]


[mit]:            https://opensource.org/licenses/MIT
[author]:         https://github.com/{{USER}}
[contributors]:   https://github.com/{{USER}}/plugin-app/graphs/contributors
[omf-link]:       https://www.github.com/oh-my-fish/oh-my-fish

[license-badge]:  https://img.shields.io/badge/license-MIT-007EC7.svg?style=flat-square
