# Always provide completions for command line utilities.
#
# Check Fish documentation about completions:
# http://fishshell.com/docs/current/commands.html#complete
#
# If your package doesn't provide any command line utility,
# feel free to remove completions directory from the project.

set commands help add update upgrade remove

complete -c app -f -d "App Manager"

complete -c app -f -n "__fish_use_subcommand" -a help -d "Show help message"
complete -c app -f -n "__fish_use_subcommand" -a add -d "Install new packages"
complete -c app -f -n "__fish_use_subcommand" -a update -d "Check for package updates"
complete -c app -f -n "__fish_use_subcommand" -a upgrade -d "Upgrade packages"
complete -c app -f -n "__fish_use_subcommand" -a remove -d "Remove packages"
complete -c app -f -n "__fish_use_subcommand" -a search -d "Search packages"
complete -c app -f -n "__fish_use_subcommand" -a list -d "Query packages"
complete -c app -f -n "__fish_use_subcommand" -a which -d "Find packages contains file"
complete -c app -f -n "__fish_use_subcommand" -a cask -d "Invoke brew cask commands [osx only]"
