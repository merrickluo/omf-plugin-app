function app -d "App Manager"
    # Package entry-point
    set argc (count $argv)
    if test $argc -eq 0
        apphelp
        return
    end

    if uname -a | grep 'Darwin' >/dev/null
        set osx 1
    end

    switch $argv[1]
        case add
            if test $argc -eq 1
                apphelp
                return
            end
            if set -q osx
                sudo port install $argv[2..-1]
            else
                pikaur -S $argv[2..-1]
            end
        case update
            if set -q osx
                sudo port selfupdate
            else
                pikaur -Sy
            end
        case upgrade
            if test $argc -eq 1
                _appdebug "Upgrade whole system"
                if set -q osx
                    sudo port upgrade outdated
                else
                    pikaur -Syu
                end
            else
                set args $argv[2..-1]
                _appdebug "Upgrade pacakges $args"
                if set -q osx
                    sudo port upgrade $args
                else
                    pikaur -Sy
                    pikaur -S $args
                end
            end
        case remove
            if test $argc -eq 1
                apphelp
                return
            end
            if set -q osx
                sudo port remove $argv[2..-1]
            else
                pikaur -R $argv[2..-1]
            end
        case search
            if test $argc -eq 1
                apphelp
                return
            end
            if set -q osx
                sudo port search $argv[2..-1]
            else
                pikaur -Ss $argv[2..-1]
            end
        case list
            if test $argc -eq 1
                if set -q osx
                    sudo port installed
                else
                    pikaur -Q
                end
            else
                if set -q osx
                    sudo port installed | grep $argv[2..-1]
                else
                    pikaur -Q $argv[2..-1]
                end
            end
        case which
            if test $argc -eq 1
                apphelp
                return
            end
            if set -q osx
                echo "not implemented"
            else
                pkgfile $argv[2..-1]
            end
        case cask
            if test $argc -eq 1
                apphelp
                return
            end
            if set -q osx
                brew cask $argv[2..-1]
            else
                echo "not implemented"
            end
        case "*"
            apphelp
    end
end

function apphelp -d "Show help for app"
    echo "Usage:
    app help                           # show this message
    app add package1 [pacakge2]        # install pacakges
    app update                         # check for updates
    app upgrade [pacakge1 package2]    # upgrade packages (omit params to upgrade whole system)
    app remove pacakge1 [package2]     # remove pacakge
    app search package                 # search package
    app list [package]                 # list installed packages (omit params for all)
    app which filename                 # findout which package provides file
    app cask [command]                 # brew cask support [osx only]
"
end

function _appdebug -d "print debug info"
    if [ $APP_DEBUG ]
        echo $argv
    end
end
